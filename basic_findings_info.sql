SELECT dojo_finding.id, Date, Severity, dojo_test_type.name as Scanner, dojo_product.name as Product
FROM dojo_finding
    INNER JOIN dojo_test
        ON dojo_finding.test_id = dojo_test.id
    INNER JOIN dojo_engagement 
        ON dojo_test.engagement_id = dojo_engagement.id 
    INNER JOIN dojo_test_type 
        ON dojo_test_type.id = test_type_id
    INNER JOIN dojo_product 
        ON dojo_product.id = dojo_engagement.product_id
WHERE dojo_finding.verified = true
