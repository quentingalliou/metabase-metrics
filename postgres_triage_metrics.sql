SELECT 
    dojo_finding.id as finding_id, 
    dojo_finding.verified as verification_sastus, 
    COALESCE(last_reviewed, last_status_update) as last_update, 
    Severity,
    dojo_test_type.name as Scanner,
    dojo_product.name as Product,
    dojo_finding.title,
    dojo_tagulous_finding_tags.name as tag
FROM dojo_finding
    INNER JOIN dojo_test
        ON dojo_finding.test_id = dojo_test.id
    INNER JOIN dojo_engagement 
        ON dojo_test.engagement_id = dojo_engagement.id 
    INNER JOIN dojo_test_type 
        ON dojo_test_type.id = test_type_id
    INNER JOIN dojo_product 
        ON dojo_product.id = dojo_engagement.product_id
    INNER JOIN dojo_finding_tags
        ON dojo_finding.id = dojo_finding_tags.finding_id
    INNER JOIN dojo_tagulous_finding_tags
        ON dojo_finding_tags.tagulous_finding_tags_id = dojo_tagulous_finding_tags.id
    
WHERE dojo_tagulous_finding_tags.name = '2nd_line_verification' or dojo_tagulous_finding_tags.name = '3rd_line_verification'
