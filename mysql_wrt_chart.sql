SELECT scan_date, Product, CASE
    WHEN Product in ('app_1')
    THEN
        Money_Risk * 0.9
    WHEN Product in ('app_2')
    THEN
        Money_Risk * 0.5
    ELSE
        Money_Risk * 1
END as Money_Risk FROM (
    SELECT scan_date, Product as Product,
    SUM(CASE
        WHEN Scanner IN ('Gitleaks Scan', 'Trufflehog Scan')
        THEN
            CASE 
                WHEN 
                    Severity = 'Low'
                THEN
                    CAST(REPLACE(Severity, 'Low', '1') AS UNSIGNED)
                WHEN 
                    Severity = 'Medium'
                THEN
                    CAST(REPLACE(Severity, 'Medium', '10') AS UNSIGNED)
                WHEN 
                    Severity = 'High'
                THEN
                    CAST(REPLACE(Severity, 'High', '25') AS UNSIGNED)
                WHEN 
                    Severity = 'Critical'
                THEN
                    CAST(REPLACE(Severity, 'Critical', '50') AS UNSIGNED)
                ELSE 0
            END
        WHEN Scanner IN ('Bandit Scan', 'PHP Security Audit v2', 'Gosec Scanner', 'SARIF', 'Sengrep JSON Report')
        THEN
            CASE 
                WHEN 
                    Severity = 'Low'
                THEN
                    CAST(REPLACE(Severity, 'Low', '1') AS UNSIGNED)
                WHEN 
                    Severity = 'Medium'
                THEN
                    CAST(REPLACE(Severity, 'Medium', '10') AS UNSIGNED)
                WHEN 
                    Severity = 'High'
                THEN
                    CAST(REPLACE(Severity, 'High', '25') AS UNSIGNED)
                WHEN 
                    Severity = 'Critical'
                THEN
                    CAST(REPLACE(Severity, 'Critical', '50') AS UNSIGNED)
                ELSE 0
            END
        WHEN Scanner IN ('Trivy Scan')
        THEN
            CASE 
                WHEN 
                    Severity = 'Low'
                THEN
                    CAST(REPLACE(Severity, 'Low', '1') AS UNSIGNED)
                WHEN 
                    Severity = 'Medium'
                THEN
                    CAST(REPLACE(Severity, 'Medium', '10') AS UNSIGNED)
                WHEN 
                    Severity = 'High'
                THEN
                    CAST(REPLACE(Severity, 'High', '25') AS UNSIGNED)
                WHEN 
                    Severity = 'Critical'
                THEN
                    CAST(REPLACE(Severity, 'Critical', '50') AS UNSIGNED)
                ELSE 0
            END
        ELSE -- Manual findings
            CASE
                WHEN 
                    Severity = 'Low'
                THEN
                    CAST(REPLACE(Severity, 'Low', '1') AS UNSIGNED)
                WHEN 
                    Severity = 'Medium'
                THEN
                    CAST(REPLACE(Severity, 'Medium', '10') AS UNSIGNED)
                WHEN 
                    Severity = 'High'
                THEN
                    CAST(REPLACE(Severity, 'High', '25') AS UNSIGNED)
                WHEN 
                    Severity = 'Critical'
                THEN
                    CAST(REPLACE(Severity, 'Critical', '50') AS UNSIGNED)
                ELSE 0
            END
        END
        ) as Money_Risk
    FROM (
        SELECT dojo_finding.id, Severity, CAST(Date as datetime) as scan_date, dojo_test_type.name as Scanner, dojo_product.name as Product
        FROM dojo_finding
            INNER JOIN dojo_test
                ON dojo_finding.test_id = dojo_test.id
            INNER JOIN dojo_engagement 
                ON dojo_test.engagement_id = dojo_engagement.id 
            INNER JOIN dojo_test_type 
                ON dojo_test_type.id = test_type_id
            INNER JOIN dojo_product 
                ON dojo_product.id = dojo_engagement.product_id
        WHERE dojo_finding.verified = true
    ) as basic_info
    GROUP BY Product, scan_date
    ORDER BY Product DESC
) as WRT 
